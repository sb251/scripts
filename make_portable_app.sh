#!/bin/bash

# $1 could either be the path or just the basename of the application
case $1 in
	*/* )
		APPLICATION=`basename $1`
		LIST_TO_SEARCH="$1"
		;;

	* ) 
		APPLICATION=$1
		LIST_TO_SEARCH=`which $1`
		if [ -z $LIST_TO_SEARCH ]; then
			echo "Binary for $1 not found in path"
			exit 1
		fi
		;;
esac

TO_COPY=""
while [ -n "$LIST_TO_SEARCH" -a "$LIST_TO_SEARCH" != " " ]; do
	BINARY=`echo $LIST_TO_SEARCH | cut -d " " -f 1`
	TO_COPY="$TO_COPY $BINARY"
	LIST_OF_DEPS=`ldd "$BINARY" | grep '=>' | sed 's/^.*=>\(.*\)(.*$/\1/'`
	LIST_TO_SEARCH=`echo $LIST_TO_SEARCH $LIST_OF_DEPS | sort | uniq | sed -e "s%\(^\| \)$BINARY\( \|$\)% %g"`
done

TARGET_DIR=`mktemp -d`
if ! [ -d $TARGET_DIR ]; then
	mkdir $TARGET_DIR
fi	
for ITEM in $TO_COPY; do
	if [ -L $ITEM ]; then
		TO_COPY="$TO_COPY `readlink -f $ITEM`"
	fi
done
rsync -a $TO_COPY $TARGET_DIR/

INSTALLER=`mktemp`
echo '#!/bin/bash
# Thanks to Matteo Mattei as this is based on http://www.matteomattei.com/create-self-contained-installer-in-bash-that-extracts-archives-and-perform-actitions/' >$INSTALLER

echo "APPLICATION=\"$APPLICATION\"" >>$INSTALLER
echo "UNPACK_DEST=\"/tmp/${APPLICATION}_portable_ext/\"" >>$INSTALLER
echo '# Create destination folder
if [ -d $UNPACK_DEST ]; then
	rm -r $UNPACK_DEST
fi
mkdir -p ${UNPACK_DEST}

# Find __ARCHIVE__ maker, read archive content and decompress it
ARCHIVE=$(awk "/^__ARCHIVE__/ {print NR + 1; exit 0; }" "${0}")
tail -n+${ARCHIVE} "${0}" | tar xpJv -C ${UNPACK_DEST}

export LD_LIBRARY_PATH="$UNPACK_DEST:$LD_LIBRARY_PATH"
$UNPACK_DEST/$APPLICATION $*
RETURN_VALUE=$?
rm -r $UNPACK_DEST
exit $RETURN_VALUE

__ARCHIVE__' >>$INSTALLER

tar -cJ -C "$TARGET_DIR" "./" >>"$INSTALLER"

# clean up
rm -r $TARGET_DIR
# gain information for final naming
. /etc/os-release
EXECUTABLE_NAME="${APPLICATION}_portable_${ID}_${VERSION_ID}"
mv $INSTALLER /tmp/$EXECUTABLE_NAME
chmod +x /tmp/$EXECUTABLE_NAME
